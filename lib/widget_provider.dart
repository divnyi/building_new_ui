import 'package:flutter/material.dart';

class IndexPath {
  final row;
  final section;
  const IndexPath(this.section, this.row);
}

typedef Widget WidgetGenerator(IndexPath path);
typedef int RowsCount(int section);
typedef int SectionsCount();

class WidgetProvider {
  final WidgetGenerator generator;
  final SectionsCount sections;
  final RowsCount rows;
  WidgetProvider(this.generator, this.sections, this.rows);

  IndexPath firstItemForSection(int section) {
    if (section >= sections()) { return null; }
    if (rows(section) == 0) { return null; }
    return IndexPath(section, 0);
  }
  bool limitsTest(IndexPath path) {
    return 0 <= path.section && path.section < sections()
    && 0 <= path.row && path.row < rows(path.section);
  }
  int totalPhysicalColumns(int shift) {
    final sectionsCount = this.sections();
    var columns = 0;
    for (int section = 0; section < sectionsCount; section++) {
      columns += this.physicalColumns(section, shift);
    }
    return columns;
  }
  int physicalColumns(int section, int shift) {
    return ((this.rows(section) * 1.0) / shift).ceil();
  }

  IndexPath nextRow(IndexPath path) {
    final newPath = IndexPath(path.section, path.row+1);
    if (limitsTest(newPath)) { return newPath; }
    return null;
  }
  IndexPath nextSection(IndexPath path) {
    for (int idx = path.section+1; idx < sections(); idx++) {
      final item = firstItemForSection(idx);
      if (item != null) { return item; }
    }
    return null;
  }
  IndexPath nextOf(IndexPath path, int shift) {
    final currentRow = ((1.0 * path.row) / shift).floor();
    if (currentRow + 1 < physicalColumns(path.section, shift)) {
      return IndexPath(path.section, (currentRow + 1)*shift);
    } else {
      return nextSection(path);
    }
  }

  IndexPath prevRow(IndexPath path) {
    final newPath = IndexPath(path.section, path.row-1);
    if (limitsTest(newPath)) { return newPath; }
    return null;
  }
  IndexPath prevSection(IndexPath path) {
    for (int idx = path.section-1; idx >= 0; idx--) {
      final item = firstItemForSection(idx);
      if (item != null) { return item; }
    }
    return null;
  }
  IndexPath prevOf(IndexPath path, int shift) {
    final currentRow = ((1.0 * path.row) / shift).floor();
    if (currentRow - 1 >= 0) {
      return IndexPath(path.section, (currentRow - 1)*shift);
    } else {
      final sectionFirst = prevSection(path);
      if (sectionFirst == null) { return null; }
      return IndexPath(sectionFirst.section, shift*(physicalColumns(sectionFirst.section, shift)-1));
    }
  }
}
