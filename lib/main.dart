import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:building_new_ui/resizable_grid.dart';
import 'package:building_new_ui/widget_provider.dart';
import 'package:building_new_ui/section_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _debugButton() {
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: this.body(context),
      floatingActionButton: FloatingActionButton(
        onPressed: _debugButton,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget body(BuildContext context) {
    final widgetProvider = WidgetProvider((path) {
        return ClipRect(
          child: DecoratedBox(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.red, Colors.green, Colors.blue],
                begin: Alignment.centerLeft,
                end: Alignment.bottomRight,
              )
            ),
            child: Center(child: Text("section = ${path.section}, row = ${path.row}"),),
          ),
        );
      }, () {
        return 5;
      }, (section) {
        return 9;
    });
    final colors = [Colors.red, null, Colors.blue, Colors.cyan,];
    final sectionProvider = ((int section) {
        Color color = (colors.length > section) ? colors[section]: null;
        if (color == null) { return null; }
        return ClipRect(
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: color
            )
          )
        );
    });
    return ResizableGrid(
      widgetProvider: widgetProvider,
      sectionProvider: sectionProvider,
      crossAxisCount: 4,
      sectionInsets: EdgeInsets.all(10.0),
      backgroundWidget: ClipRect(
        child: DecoratedBox(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.black],
              begin: Alignment.centerLeft,
              end: Alignment.bottomRight,
            )
          ),
        ),
      ),
    );
  }
}
