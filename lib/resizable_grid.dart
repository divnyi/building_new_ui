import 'package:flutter/material.dart';
import 'dart:math';
import 'package:building_new_ui/widget_provider.dart';
import 'package:building_new_ui/section_provider.dart';

class ResizableGrid extends StatefulWidget {
  final WidgetProvider widgetProvider;
  final SectionProvider sectionProvider;
  final Widget backgroundWidget;
  final Color backgroundColor;
  final EdgeInsets sectionInsets;
  final double widthToHeight;
  final int crossAxisCount;
  final int minCrossAxisCount;
  final int maxCrossAxisCount;
  final double alignIfLessThanNeededColumns;
  final double alignIfLessThanNeededRows;
  final Axis portraitAxis;
  final Axis landscapeAxis;
  final double scrollFocalPointAdvancementMultiplier;
  ResizableGrid({
      @required this.widgetProvider,
      this.sectionProvider,
      this.sectionInsets = const EdgeInsets.all(0.0),
      this.backgroundWidget,
      this.backgroundColor = Colors.black,
      this.widthToHeight = 1.0,
      this.crossAxisCount = 2,
      this.alignIfLessThanNeededColumns = 0.5,
      this.alignIfLessThanNeededRows = 0.5,
      this.minCrossAxisCount = 1,
      this.maxCrossAxisCount = 20,
      this.portraitAxis = Axis.vertical,
      this.landscapeAxis = Axis.horizontal,
      this.scrollFocalPointAdvancementMultiplier = 1.4,
  });

  @override
  State<StatefulWidget> createState() {
    return ResizableGridState();
  }
}

typedef void Callback();
class ResizableGridState extends State<ResizableGrid> {
  Axis currentAxis = Axis.horizontal;
  List<List<IndexPath>> gridPaths;
  List<Positioned> cells;
  IndexPath firstElement;
  IndexPath get lastElement => gridPaths != null ? gridPaths.last.last : null;
  int get currentPhysicalColumns => gridPaths.length;
  int crossAxisCount;
  double firstItemHiddenPoints;

  double width;
  double height;
  Offset topLeft;
  Orientation orientation;
  double get itemHeight => this.width != null ? ((this.currentAxis == Axis.horizontal) ? height / this.crossAxisCount : itemWidth * widget.widthToHeight) : null;
  double get itemWidth => this.width != null ? ((this.currentAxis == Axis.horizontal) ? itemHeight / widget.widthToHeight : width / this.crossAxisCount) : null;

  // axis-agnostic
  double get scrollingItemSize => this.currentAxis == Axis.horizontal ? itemWidth : itemHeight;
  double get crossItemSize => this.currentAxis == Axis.horizontal ? itemHeight : itemWidth;
  double get scrollingScreenSize => this.currentAxis == Axis.horizontal ? width : height;
  double get crossScreenSize => this.currentAxis == Axis.horizontal ? height : width;
  int get maxColumns => (scrollingScreenSize / scrollingItemSize).ceil();
  int get physicalColumns => ((scrollingScreenSize + firstItemHiddenPoints - totalShift) / scrollingItemSize).ceil();
  int get physicalRows => crossAxisCount;
  double get sectionHeader => this.currentAxis == Axis.horizontal ? widget.sectionInsets.left : widget.sectionInsets.top;
  double get sectionFooter => this.currentAxis == Axis.horizontal ? widget.sectionInsets.right : widget.sectionInsets.bottom;
  double get sectionFlowOffset1 => this.currentAxis == Axis.horizontal ? widget.sectionInsets.top : widget.sectionInsets.left;
  double get sectionFlowOffset2 => this.currentAxis == Axis.horizontal ? widget.sectionInsets.bottom : widget.sectionInsets.right;
  List<double> headerForColumn = List<double>();
  List<double> footerForColumn = List<double>();
  List<double> shiftForColumn = List<double>();
  List<int> gridSectionBegins = List<int>();
  double totalShift = 0.0;
  double recenterShift = 0.0;
  bool get lastItemIsVisible => this.lastElement == null ? false : ((widget.widgetProvider.sections() - 1) == this.lastElement.section && (widget.widgetProvider.rows(this.lastElement.section) - 1) == this.lastElement.row);
  bool get firstItemIsVisible => this.firstElement.section == 0 && this.firstElement.row == 0;

  Offset scaleFocalPoint;
  double scalingFactor;
  Offset panLastPoint;

  List<Callback> onLayout = List<Callback>();

  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(afterLayout);
    super.initState();
    this.firstItemHiddenPoints = 0.0;
    this.crossAxisCount = widget.crossAxisCount;
    this.firstElement = IndexPath(0, 0);
  }

  /// scroll the grid so item at path would be in the first column
  void scrollToFirstColumn(IndexPath path) {
    if (widget != null && this.width != null) {
      final firstRowInColumn = ((1.0 * path.row) / this.crossAxisCount).floor() * this.crossAxisCount;
      final firstRowInColumnPath = IndexPath(path.section, firstRowInColumn);
      setState(() {
          this.firstElement = firstRowInColumnPath;
          this.firstItemHiddenPoints = 0.0;
          this.gridPaths = null;
          this.shiftForColumn = List<double>();
          this.footerForColumn = List<double>();
          this.headerForColumn = List<double>();
          this.totalShift = 0.0;
      });
    }
  }

  /// scroll the grid so item at path would be in the destinationColumn column
  void scrollToItem(IndexPath path, int destinationColumn) {
    if (widget != null && this.width != null) {
      final firstRowInColumn = ((1.0 * path.row) / this.crossAxisCount).floor() * this.crossAxisCount;
      final firstRowInColumnPath = IndexPath(path.section, firstRowInColumn);
      var firstElt = firstRowInColumnPath;
      for (int i = 0; i < destinationColumn; i++) {
        final newFirst = widget.widgetProvider.prevOf(firstElt, this.crossAxisCount);
        if (newFirst == null) {
          break;
        }
        firstElt = newFirst;
      }
      scrollToFirstColumn(firstElt);
    }
  }

  /// scroll the grid so item at path would be in middle column
  void scrollToMiddleColumn(IndexPath path) {
    if (widget != null && this.width != null) {
      final columns = this.maxColumns;
      scrollToItem(path, ((columns-1)/2).floor());
    }
  }

  /// scroll the grid so item at path would be in last column
  void scrollToLastColumn(IndexPath path) {
    if (widget != null && this.width != null) {
      final columns = this.maxColumns;
      scrollToItem(path, columns);
    }
  }

  void afterLayout(_) {
    this.setState(() { }); // reload to obtain true width and height
  }

  bool lateInitRun = false;
  bool afterLayoutRun = false;
  void lateInit(BuildContext context) {
    if (!lateInitRun) {
      lateInitRun = true;
      // init stuff that needs context
    }
    if (!afterLayoutRun) {
      final RenderBox renderer = context.findRenderObject();
      if (renderer is RenderBox
        && renderer != null
        && renderer.hasSize) {
        Future.delayed(Duration(milliseconds: 1), () {
            setState(() {
                afterLayoutRun = true;

                // init stuff that needs renderer
                this.width = renderer.size.width;
                this.height = renderer.size.height;
                this.topLeft = renderer.localToGlobal(Offset(0, 0));

                this.onLayout.forEach((callback) {
                    callback();
                });
                this.onLayout.clear();
            });
        });
      }
    }
  }

  void addOnLayoutCallback(Callback callback) {
    this.afterLayoutRun = false;
    this.onLayout.add(callback);
    Future.delayed(Duration(milliseconds: 1), () {
        this.setState(() { });
    });
  }

  @override
  Widget build(BuildContext context) {
    this.lateInit(context);
    return gestures(context,
      orientationAware(context));
  }

  Widget gestures(BuildContext context, Widget child) {
    return GestureDetector(
      onScaleStart: (details) {
        this.panLastPoint = details.focalPoint;
      },
      onScaleUpdate: (details) {
        if (details.scale == 1.0) { // it's not scale, but single finger pan
          setState(() {
              final delta = details.focalPoint - (panLastPoint ?? details.focalPoint);
              this.panLastPoint = details.focalPoint;
              if (this.currentAxis == Axis.vertical) {
                this.firstItemHiddenPoints -= delta.dy;
              } else {
                this.firstItemHiddenPoints -= delta.dx;
              }
              this.scaleFocalPoint = null;
              this.scalingFactor = null;
          });
          return;
        }

        setState(() {
            this.scaleFocalPoint = details.focalPoint - this.topLeft;
            this.scalingFactor = details.scale;
            this.panLastPoint = null;
        });
      },
      onScaleEnd: (details) {
        setState(() {
            if (this.scaleFocalPoint != null) {
              final indexPath = getIndexPathAtPosition(this.scaleFocalPoint);
              if (this.scalingFactor != null) {
                this.crossAxisCount =
                min(widget.maxCrossAxisCount,
                  max(widget.minCrossAxisCount,
                    (this.crossAxisCount / this.scalingFactor).round()));
              }
              scrollToMiddleColumn(indexPath);
            }

            this.scalingFactor = null;
            this.scaleFocalPoint = null;
            this.panLastPoint = null;
        });
      },
      child: child,
    );
  }

  Widget orientationAware(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
        if(this.orientation != orientation) {
          final oldItemWidth = this.itemWidth;
          final oldItemHeight = this.itemHeight;
          final capturedFirstItem = this.firstElement;
          this.height = null;
          this.width = null;
          this.addOnLayoutCallback (() {
              this.setState(() {
                  this.firstElement = capturedFirstItem;
                  this.firstItemHiddenPoints = 0.0;
                  this.currentAxis = this.orientation == Orientation.portrait ? widget.portraitAxis : widget.landscapeAxis;
                  if (oldItemHeight != null && oldItemWidth != null) {
                    var newCrossAxisCount;
                    if (this.currentAxis == Axis.horizontal) {
                      newCrossAxisCount = (this.height / oldItemHeight).round();
                    } else {
                      newCrossAxisCount = (this.width / oldItemWidth).round();
                    }
                    this.crossAxisCount = newCrossAxisCount;
                  }
              });
          });
        }
        this.orientation = orientation;
        return stack(context);
    },);
  }

  Widget stack(BuildContext context) {
    if (this.height == null || this.width == null) {
      // widget is building but is still not layouted
      return Container();
    }
    final positionedWidgets = positioning();

    return Stack(
      alignment: Alignment.topLeft,
      overflow: Overflow.visible,
      children: positionedWidgets
    );
  }

  List<Positioned> positioning() {
    this.positioningMath();
    
    var positionedWidgets = List<Positioned>();
    Widget background = Positioned(
      left: 0,
      top: 0,
      width: width,
      height: height,
      child: Container(color: widget.backgroundColor,)
    );
    positionedWidgets.add(background);
    Widget backgroundWidget = widget.backgroundWidget;
    if (backgroundWidget != null) {
      Widget background = Positioned(
        left: 0,
        top: 0,
        width: width,
        height: height,
        child: backgroundWidget
      );
      positionedWidgets.add(background);
    }

    print("$gridSectionBegins");
    for (int sectionIdx = 0; sectionIdx < this.gridSectionBegins.length; sectionIdx++) {
      final column = this.gridSectionBegins[sectionIdx];
      final section = this.gridPaths[column].first.section;
      final child = widget.sectionProvider(section);

      if (child == null) { continue; }

      final lastColumn = (this.gridSectionBegins.length > sectionIdx+1) ? (this.gridSectionBegins[sectionIdx+1]-1) : this.gridPaths.length-1;
      final shiftBySectionInsets = this.shiftForColumn[column];
      final scrollingOffset = column*scrollingItemSize - this.firstItemHiddenPoints + recenterShift + shiftBySectionInsets - this.headerForColumn[column];
      final scrollingWidth = scrollingItemSize*(lastColumn-column+1)+this.footerForColumn[lastColumn] + this.headerForColumn[column];

      final topLeft = this.currentAxis == Axis.horizontal ? Offset(scrollingOffset, 0.0) :  Offset(0.0, scrollingOffset);
      final size = this.currentAxis == Axis.horizontal ? Size(scrollingWidth, crossScreenSize) : Size(crossScreenSize, scrollingWidth);

      final positioned = Positioned(
        top: topLeft.dy,
        left: topLeft.dx,
        width: size.width,
        height: size.height,
        child: child
      );
      positionedWidgets.add(positioned);
    }

    var cells = List<Positioned>();
    for (int column = 0; column < gridPaths.length; column++) {
      var rowShift = 0.0;
      final missingRows = this.physicalRows - this.gridPaths[column].length;
      if (missingRows > 0) {
        rowShift = (crossScreenSize - (this.gridPaths[column].length * crossItemSize)) * widget.alignIfLessThanNeededRows;
      }
      final shiftBySectionInsets = this.shiftForColumn[column];
      final scrollingOffset = column*scrollingItemSize - this.firstItemHiddenPoints + recenterShift + shiftBySectionInsets;

      for (int row = 0; row < gridPaths[column].length; row++) {
        final crossOffset = row*crossItemSize + rowShift;
        final indexPath = gridPaths[column][row];
        final child = widget.widgetProvider.generator(indexPath);
        Offset scaleOffset = Offset(0, 0);
        double scaling = this.scalingFactor ?? 1.0;
        final topLeft =
        this.currentAxis == Axis.horizontal ?
        Offset(scrollingOffset, crossOffset) :
        Offset(crossOffset, scrollingOffset);
        if (this.scalingFactor != null && this.scaleFocalPoint != null) {
          Offset middlePoint = Offset(width/2, height/2);
          Offset focalMidDiff = this.scaleFocalPoint - middlePoint;
          Offset advancedFocalPoint = middlePoint + (focalMidDiff*widget.scrollFocalPointAdvancementMultiplier);
          final shiftFromFocal = topLeft - advancedFocalPoint;
          final scaledShift = shiftFromFocal * scaling;
          final newTopLeft = this.scaleFocalPoint + scaledShift;
          scaleOffset = newTopLeft - topLeft;
        }
        final positioned = Positioned(
          top: topLeft.dy + scaleOffset.dy,
          left: topLeft.dx + scaleOffset.dx,
          width: itemWidth*scaling,
          height: itemHeight*scaling,
          child: child
        );
        positionedWidgets.add(positioned);
        cells.add(positioned);
      }
    }
    this.cells = cells;
    return positionedWidgets;
  }

  void positioningMath() {
    recenterShift = 0.0;
    for (int tries = 0; tries < 100; tries++) {
      final firstHeader = (this.headerForColumn.length > 0 ? this.headerForColumn[0] : 0.0) ?? 0.0;
      final firstFooter = (this.footerForColumn.length > 0 ? this.footerForColumn[0] : 0.0) ?? 0.0;
      var addZeroItemInsets = false;
      var changes = false;
      print("points: ${firstItemHiddenPoints}");
      if (this.gridPaths == null
        || this.shiftForColumn == null) {
        changes = true;
      } else if (firstItemIsVisible && lastItemIsVisible) {
        print("trigger0");
        final contentSize = currentPhysicalColumns * scrollingItemSize
          + this.footerForColumn.reduce((sum, item) { return sum+item; })
          + this.headerForColumn.reduce((sum, item) { return sum+item; });
        if (contentSize < scrollingScreenSize) {
          this.firstItemHiddenPoints = 0; // content is smaller than screen
          final oldRecenterShift = recenterShift;
          recenterShift = (scrollingScreenSize - contentSize) * widget.alignIfLessThanNeededColumns;
          if (oldRecenterShift != recenterShift) {
            changes = true;
          }
        } else {
          this.firstItemHiddenPoints = min((contentSize - scrollingScreenSize), max(0.0, this.firstItemHiddenPoints));
        }
      } else if (this.firstItemHiddenPoints >= scrollingItemSize + firstHeader + firstFooter) {
        changes = true;
        print("trigger1");
        final newFirstWidget = widget.widgetProvider.nextOf(this.firstElement, this.crossAxisCount);
        if (newFirstWidget == null) {
          this.firstItemHiddenPoints = 0.0;
        } else {
          this.firstElement = newFirstWidget;
          this.firstItemHiddenPoints -= scrollingItemSize + firstHeader + firstFooter;
        }
      } else if (this.firstItemHiddenPoints < 0.0) {
        changes = true;
        print("trigger2");
        final newFirstWidget = widget.widgetProvider.prevOf(this.firstElement, this.crossAxisCount);
        if (newFirstWidget == null) {
          this.firstItemHiddenPoints = 0.0;
        } else {
          this.firstElement = newFirstWidget;
          this.firstItemHiddenPoints += scrollingItemSize;
          addZeroItemInsets = true;
        }
      } else if (lastItemIsVisible) {
        final column = this.gridPaths.length - 1;
        final shiftBySectionInsets = this.shiftForColumn[column];
        final scrollingOffset = column*scrollingItemSize - this.firstItemHiddenPoints + shiftBySectionInsets;
        final rightmostPosition = scrollingOffset + scrollingItemSize + this.footerForColumn[column];
        final difference = scrollingScreenSize - rightmostPosition;
        if (difference > 0) {
          // scrolled beyond last item
          changes = true;
          this.firstItemHiddenPoints -= difference;
          this.firstItemHiddenPoints = this.firstItemHiddenPoints.floor()*1.0;
        }
        print("points: ${firstItemHiddenPoints} diff: ${difference}");
      }

      this.gridPaths = this.gridForFirstItem(); // (re)calculate
      this.calculateSections();

      if (addZeroItemInsets) {
        final firstHeader = (this.headerForColumn.length > 0 ? this.headerForColumn[0] : 0.0) ?? 0.0;
        final firstFooter = (this.footerForColumn.length > 0 ? this.footerForColumn[0] : 0.0) ?? 0.0;
        this.firstItemHiddenPoints += firstHeader + firstFooter;
      }
      if (!changes) {
        print("early exit");
        break;
      }
      if (tries == 99) {
        print("tryhard");
      }
    }
  }
  
  /// get indexPath (as in widgetProvider) of item, or any item that is close if none at that point
      IndexPath getIndexPathAtPosition(Offset hit) {
        if ( this.width == null
          || this.gridPaths == null
          || this.cells == null) { return null; }

        IndexPath bestItemIndex;
        double bestDistance;
        int idx = 0;
        for (int column = 0; column < this.gridPaths.length; column++) {
          for (int row = 0; row < this.gridPaths[column].length; row++) {
            final pos = this.cells[idx];
            Offset itemCenter = Offset(pos.left+(pos.width/2), pos.top+(pos.height/2));
            final distance = pow(pow((itemCenter.dx-hit.dx), 2)+pow((itemCenter.dy-hit.dy), 2), 0.5);
            if (bestDistance == null || bestDistance > distance) {
              bestDistance = distance;
              bestItemIndex = this.gridPaths[column][row];
            }
            idx++;
          }
        }
        print("best: ${bestItemIndex.section}:${bestItemIndex.row}");
        return bestItemIndex;
      }

  List<IndexPath> rowForFirstItem(IndexPath path) {
    var rowPaths = List<IndexPath>();
    for (int physicalRow = 0; physicalRow < physicalRows; physicalRow++) {
      if (path == null) {
        break;
      }
      rowPaths.add(path);
      path = widget.widgetProvider.nextRow(path);
    }
    return rowPaths;
  }

  List<List<IndexPath>> gridForFirstItem() {
    final physicalColumns = this.physicalColumns;
    if (physicalColumns >= widget.widgetProvider.totalPhysicalColumns(this.crossAxisCount)) {
      this.firstElement = IndexPath(0, 0);
    }
    this.firstElement = IndexPath(this.firstElement.section, (this.firstElement.row * 1.0 / crossAxisCount).floor() * crossAxisCount);
    var currentPath = this.firstElement;
    var paths = List<List<IndexPath>>();
    for (int physicalColumn = 0; physicalColumn < physicalColumns; physicalColumn++) {
      if (currentPath == null) {
        break;
      }
      var rowPaths = rowForFirstItem(currentPath);
      paths.add(rowPaths);
      currentPath = widget.widgetProvider.nextOf(currentPath, crossAxisCount);
    }
    return paths;
  }

  void calculateSections() {
    var headerShift = List<double>();
    var footerShift = List<double>();

    var columnShift = List<double>();
    var sectionBegins = List<int>();
    var totalShift = 0.0;

    for (int column = 0; column < this.gridPaths.length; column++) {
      final current = gridPaths[column].first;
      final previous = widget.widgetProvider.prevOf(current, crossAxisCount);
      final next = widget.widgetProvider.nextOf(current, crossAxisCount);

      var header = 0.0;
      var footer = 0.0;
      if (sectionBegins.length == 0 ||
        this.gridPaths[sectionBegins.last].first.section != current.section) {
        sectionBegins.add(column);
      }
      if (previous == null || previous.section != current.section) {
        header = this.sectionHeader;
        totalShift += header;
        if (column > 0) {
          totalShift += footerShift[column-1];
        }
      }
      if (next == null || next.section != current.section) {
        footer = this.sectionFooter;
      }
      headerShift.add(header);
      footerShift.add(footer);

      columnShift.add(totalShift);
    }
    if (footerShift.length > 0) {
      final column = columnShift.length - 1;
      final shiftBySectionInsets = columnShift.last;
      final scrollingOffset = (column+1)*scrollingItemSize - this.firstItemHiddenPoints + shiftBySectionInsets;
      if (scrollingOffset < scrollingScreenSize) {
        totalShift += footerShift.last;
      }
    }
    this.headerForColumn = headerShift;
    this.footerForColumn = footerShift;

    this.shiftForColumn = columnShift;
    this.totalShift = totalShift;

    this.gridSectionBegins = sectionBegins;
  }
}
